### About

This sample **Spring Boot** project creates an *Netflix/Eureka* server for **Service Discovery** by using `@EnableEurekaServer` annotation. 

---

### Code Example

`@EnableEurekaServer` annotation is used on the application main class to make the application an **Eureka server**.


**Application Main class** (`com.example.es.EurekaServerApplication`): the only class in the project.

```java
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}

}
```

---

** Property file** (`application.yml`): since the server is configured to run on the default port `server.port=8761`, this property is in fact, *redundant*, but is given nonetheless. `eureka.client.register-with-eureka=false` property is used so that this application acts only as **Eureka Server** and does not register itself as **Eureka Client**. `eureka.client.fetch-registry=false` specifies that other **Eureka Clients** will not be able to fetch its registry information. 

```yml
server:
  port: 8761
  
eureka:
  client:
    register-with-eureka: false    
    fetch-registry: false
```

---

** Execution log**: an example execution log which shows ... 

```
... [TODO]
```

---

### Reference Documentation
For further reference, please consider the following sections:

* [Service Registration and Discovery](https://spring.io/guides/gs/service-registration-and-discovery/)
* [Netflix/Eureka](https://github.com/Netflix/eureka)

